import webapp2 


class IndexResource(webapp2.RequestHandler): 

    def get(self): 
        self.response.headers['Content-Type'] = 'text/plain' 
        self.response.write('OK') 

APP = webapp2.WSGIApplication([ ('/', IndexResource) ], debug=True)